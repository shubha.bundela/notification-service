import AWS from "aws-sdk";
import mimemessage from "mimemessage";
import middleware from "../../lib/middleware";
import { config } from "./config";

let mailContent = mimemessage.factory({
  contentType: "multipart/mixed",
  body: [],
});

AWS.config.update({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion,
});
const SES = new AWS.SES({
  region: "us-east-1",
});
/**
 * @param {*} event
 * @param {*} context
 * @return {*}
 */

async function sendMail(event, context) {
  console.log("event------------", event);
  let { csv } = event.body;
  csv = JSON.parse(csv);
  console.log("csv", csv);
  let attachmentEntity = mimemessage.factory({
    contentType: "text/plain",
    body: csv,
  });

  attachmentEntity.header(
    "Content-Disposition",
    'attachment ;filename="chatbot-report.csv"',
  );
  mailContent.body.push(attachmentEntity);
  mailContent.header("From", "supportchatbot@yopmail.com");
  mailContent.header("To", "shubha.bundela@harbingergroup.com");
  mailContent.header("Subject", "Chatbot Report");

  try {
    const result = await SES.sendRawEmail({
      RawMessage: { Data: mailContent.toString() },
    }).promise();
    console.log("Check result here: ", result);
  } catch (e) {
    console.log("error", e);
  }
}

export const handler = middleware(sendMail);
